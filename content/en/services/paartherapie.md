---
title: 'Paartherapie'
discription: 'Ein wichter Beitrag für Mitarbeiter und Betrieb'
date: 2020-01-01
menu:
summary: '
Die meisten Menschen leben in einer Zweierbeziehung daher ist diese intensive direkte Beziehung auch das Feld in dem alle unsere Einstellungen, Beschränkungen und Wünsche am deutlichsten werden.'
categories: [one]
tags: [two,three,four]
image:
draft: true
featured: true
weight: 5
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# Was ist Paartherapie

Die Paartherapie hilft Ihnen dabei, festgefahrene Kommunikationsmuster zu unterbrechen. Ich unterstütze Sie in Dialog zu kommen und den Teufelskreis aus Streit, Distanz oder Sprachlosigkeit zu beenden. Beide Partner haben die Möglichkeit, ihre eigene Sichtweise darzustellen. Gemeinsam werden alte Verhaltensweisen und Verletzungen bearbeitet. Alte Wunden können heilen.
