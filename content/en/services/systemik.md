---
title: 'Systemische Therapie'
discription:
date: 2020-01-01
menu:
summary: '
Menschen sind immer Teil von den sie umgebenen Systemen. Diese Wechselwirkungen können beeinflusst werden. Als erster Schritt dient dazu die Bewustwerdung. Eine direkte Verändern des Erlebens dieser Zusammenhänge ist der nächste Schritt. Dabei spielt das körperliche Erleben eine große Rolle.'
categories: [one]
tags: [two,three,four]
image:
draft: true
featured: true
weight: 2
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Systemische Therapie

oder Systemische Familientherapie ist ein psychotherapeutisches Verfahren dessen Schwerpunkt auf dem sozialen Kontext psychischer Störungen, insbesondere auf Interaktionen zwischen Mitgliedern der Familie und deren sozialer Umwelt liegt. In Abgrenzung zur Psychoanalyse betonen Vertreter dieser Therapierichtung die Bedeutung impliziter Normen des Zusammenlebens für das Zustandekommen und die Überwindung psychischer Störungen (Familienregeln).

Allerdings berücksichtigen auch andere Therapieformen wie zum Beispiel die Kognitive Kurzzeittherapie den ‘systemischen’ Aspekt. Die Systemische Therapie unterscheidet sich nach Angaben ihrer Vertreter dadurch, dass weitere Mitglieder des für den Patienten relevanten sozialen Umfeldes in die Behandlung einbezogen werden.

Seit Ende 2008 wird in Deutschland die Systemische Therapie als wissenschaftliches Psychotherapieverfahren anerkannt.
