---
title: 'Individual therapy'
discription: ''
date: 2020-01-01
menu:
summary: '
Psychotherapy in the classic two-person constellation with a client and a therapist is probably the most intensive relationship that people enter into in addition to the love relationship. Even if the primary concern of the client is a mutual human connection of great intensity.
'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 12
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Individual therapy.

Psychotherapy in the classic two-person constellation with a client and a therapist is probably the most intensive relationship that people enter into in addition to the love relationship. Even if it is primarily about the client's concern, it is the mutual human connection of great intensity.
