---
title: 'Familientherapie'
discription: ''
date: 2020-01-01
menu:
summary: '
Familienprobleme sind normal: Zu Hause ist schlechte Stimmung oder es fliegen sogar die Fetzen. Ihnen wird manchmal alles zu viel, Sie können die dauernden Streitigkeiten in Ihrer Familie nicht mehr ertragen und fühlen sich selbst überfordert und ein Stück weit »ungeliebt« und »unverstanden«.'
categories: [one]
tags: [two,three,four]
image:
draft: true
featured: true
weight: 1
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# Was ist Familientherapie

Familienprobleme sind normal: Zu Hause ist schlechte Stimmung oder es fliegen sogar die Fetzen. Ihnen wird manchmal alles zu viel, Sie können die dauernden Streitigkeiten in Ihrer Familie nicht mehr ertragen und fühlen sich selbst überfordert und ein Stück weit »ungeliebt« und »unverstanden«. Sie möchten manchmal am liebsten alles hinwerfen. In jeder Familie gibt es hin und wieder Schwierigkeiten, die den Familienmitgliedern Rücksichtnahme, Verständnis und die Bereitschaft, dem anderen zuzuhören, abverlangen. Dafür brauchen Sie keinen Familientherapeuten. Manchmal ist es jedoch unmöglich, innerhalb der
Familie wieder zusammenfinden, oder es gibt ernste Probleme, wenn es um Fragen wie diese geht:

In Ihrer Familie gibt es schwere Belastungen – zum Beispiel Krankheit, Pflegebedürftigkeit, Alkoholmissbrauch, Depression, den Ausschluss eines Familienmitglieds beziehungsweise »schwarzen Schafes« –, die auch Sie als Familienmitglied berühren und die
Sie aufarbeiten möchten.

* Sie müssen in der Familie gerade mit Tod, Trauer, Scheidung oder Ähnlichem umgehen. Sie möchten Ihre Kinder in dieser Phase besser unterstützen.

* In Ihrer Familie gibt es häufig Streit und ernste Konflikte, in denen Sie Ihre Position finden möchten.

* Sie möchten die Beziehung zu Ihren Eltern beziehungsweise Geschwistern verbessern.

* Wie sollten Sie mit den Schwierigkeiten Ihres Kindes in der Schule umgehen?

* Ist es eine gute Idee, während der Arbeitszeit die Kinder zu den Schwiegereltern zu bringen?

* In Ihrer Partnerschaft gibt es in letzter Zeit Probleme. Wird Ihr Kind darunter leiden?

* In Ihrer Familie gibt es schwer verständliche destruktive Muster, und das über Generationen. Sie möchten gerne herausfinden, was es damit auf sich hat.

* Ihre Schwiegereltern mischen sich in Ihr Leben und in die Erziehung Ihres Kindes ein.

* Ihre Eltern verstehen Sie nicht und wollen im Grunde einen anderen Menschen aus Ihnen machen.

* Sie möchten schwierige Ereignisse, Geheimnisse oder Tabus in Ihrer Familiengeschichte aufarbeiten.

*  Ihre Familie hat Ihnen innere Muster und Überzeugungen mitgegeben, die Sie heute noch hemmen beziehungsweise belasten und die sich als Problem in Ihrer Partner-/ Elternschaft fortpflanzen. Diesen Kreis möchten Sie durchbrechen.

Wenn Sie Fragen wie diese bewegen, empfiehlt es sich, eine Familienberatung beziehungsweise eine Familientherapie in Anspruch zu nehmen, um die anstehenden Probleme zu lösen.
