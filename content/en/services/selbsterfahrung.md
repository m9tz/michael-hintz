---
title: 'Selbsterfahrung'
discription:
date: 2020-01-01
menu:
summary: '
Ein Begriff der am Anfang steht. Nach einer umfangreichen Suche nach Erkenntnis in der Jugend, geprägt von intensiven LSD Erfahrungen und ersten Begegnungen mit dem tibetischen Buddhismus. Lernte ich wie es zu der Zeit Mainstream war zunächst einige Methoden von Therapeuten kennen die damals enge Schüler von [Werner Erhard](http://wernererhard.de/biography.html) waren, der Begründer des EST-Trainings. Aber auch Therapeuten aus dem Umfeld von [Osho](https://de.wikipedia.org/wiki/Osho).'
categories: [one]
tags: [two,three,four]
image:
draft: true
featured: true
weight: 10
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Eigene Selbsterfahrung

Ein Begriff der am Anfang steht. Nach einer umfangreichen Suche nach Erkenntnis in der Jugend, geprägt von intensiven LSD Erfahrungen und ersten Begegnungen mit dem tibetischen Buddhismus. Lernte ich wie es zu der Zeit Mainstream war zunächst einige Methoden von Therapeuten kennen die damals enge Schüler von [Werner Erhard](http://wernererhard.de/biography.html) waren, der Begründer des EST-Trainings. Aber auch Therapeuten aus dem Umfeld von [Osho](https://de.wikipedia.org/wiki/Osho).

Einen wirklich sehr intensiven und damals prägenden Eindruck hinterließ aber eine Methode Names [Enlightenment Intensive](https://de.wikipedia.org/wiki/Enlightenment_Intensive) die von dem Yogi und kalifornischen Kommunikationswissenschaftler [Charles Berner](http://www.charlesberner.org/) entwickelt wurde. Diese Seminarform verbindet wie keine andere östliche Philosophie mit moderner Kommunikation und Psychotherapie, sie beinhaltet die intensive Auseinandersetzung mit der Frage "Sag mir wer du bist?" und "Sag mir was ein andere ist?".

Zeitgleich machte ich Erfahrungen mit dem [Feuerlaufen](https://de.wikipedia.org/wiki/Feuerlauf). Meinen ersten Gehversuche mit eigenen Gruppen drehten sich um dieses Thema.

Meine erste umfangreiche Ausbildung in einer Psychotherapie Methode war die Hakomi-Methode. Eine Körperorientierte Methode die ich in einem [eigenen Artikel](/services/hakomi/) näher erläutere. Ich hatte Glück den Begründer der Methode sehr persönlich kennen zu lernen. Ron Kurz hat zum Ende seines Lebens sehr intensive an der therapeutische Beziehung als den eigentlich tragenden Pfeiler jeder Therapie Form gearbeitet und in seinem letzten nicht mehr fertiggestellten Buch zusammengetragen.

Die zweite Methode die ich studierte ist die Systemische Therapie die auch eine [eigene Darstellung](services/systemik) hat.
