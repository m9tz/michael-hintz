---
title: 'Yoga und Therapie'
discription: ''
date: 2020-01-01
menu:
summary: '
Yoga ist heute Live-Style und in aller Munde. Warum ich trotzden noch was dazu beitragen möchte: Yoga führt zur Erleuchtung! Dabei wird man nebenbei vielleicht auch noch gesund bevor man stirbt.'
categories: [one]
tags: [two,three,four]
image:
draft: true
featured: true
weight: 3
weight_front:
weight_print:
# intro_image: images/undraw/undraw_yoga_248n.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Yoga und Therapie

Yoga ist heute Live-Style und in aller Munde. Warum ich trotzdem noch was dazu beitragen möchte: Yoga führt zur Erleuchtung! Dabei wird man nebenbei vielleicht auch noch gesund bevor man stirbt.

## Yoga wie ich es verstehe

1990 hieß mein erster Yoga-Lehrer in Lübeck "Amnuai" und kam aus Thailand. Er folgte einer buddhistischen Yogaline. Seit vielen Jahren folge ich nicht nur in Sachen Yoga dem [Yantra-Yoga](http://www.yantrayoga.net/), einer tibetischen Tradition die zuerst von [Chögyal Namkhai Norbu](http://dodjungling.de/choegyal-namkhai-norbu/) in den Westen gebracht wurde.

Yoga ermöglicht uns den Atem bewusst zu lenken und damit auch das Bewusstsein. Der Atem ist die Schnittstelle zwischen physischem Körper und dem energetischem Geschehen in uns. Diese beiden Ebenen sind es die auch in der Psychotherapie angesprochen werden, dort oft durch Sprache und inneres Erleben. In körperorientierten Methoden wie dem Hakomi aber auch direkt über den Körper. Yoga ist weder mit dem Hinduismus noch mit dem Buddhismus verbunden, sonder ist ein eigenständiger Weg und als solcher auch auch als Lifestyle lehrbar - warum nicht? Es gibt viele Yoga-Traditionen auch recht exotisch anmutende wie [Tensegrity®](https://www.youtube.com/watch?v=LukImQbfTCY) oder [Tumo/Innere Hitze](https://www.youtube.com/watch?v=BESrdlf-cPg) allen gemeinsam ist aber, dass es nicht vorrangig um Gesundheit geht sondern um Erkenntnis.

## Yoga als Selbsterfahrung

Jede Erfahrung die bewusst wird ändert das Bewusstsein und gibt ein positives Feedback das dazu führt die natürliche innere Achtsamkeit weiter zu praktizieren. Daher geht es bei der sogenannte Yogatherapie oder Yoga als Selbsterfahrung nicht um den körperlichen Aspekt der Yogaübung. Es geht um die Bewustwerdung der eigenen Wahrnehmung vom inneren und äußeren Eindrücken und letztlich um die Auflösung dieser festen anmutenden Trennung. Der körperliche Aspekt ist unbestritten und hat als solches natürlich seinen Wert, ist aber nichts was alleiniger Zweck die Motivation ausmachen sollte - dann ist Gymnastik und Sport bestimmt sinnvoller.

## Ausgleichen der Elemente durch Yoga

Die Vorstellung von dem Gleichgewicht der Elemente in unserem Körper ist ein grundlegendes Prinzip fast aller alten Medizin Traditionen. Die Vorstellung das Körper und Geist einander beeinflussen auch. Wie Yoga in diese Wechselwirkung eingreift und auch gezielt eingesetzt wird ist in einigen Büchern gut beschrieben. Hier aber noch ein andere Aspekt der Elemente die im Yoga auch berührt werden kann. Die im außen auftretenden Elemente sind in Verbindung mit den inneren Elementen. Wir kennen das allen, eine Sonnenreiche Umgebung oder eine Windreiche Umgebung wirken sehr unterschiedlich auf uns. Aber auch der durch Visualisierung generierter Kontakt zu einem Element ändert unser Erleben und wirkt bis auf die Psyche. Viele Traditionen sehen die Ebene der Elemente als die eigentliche Verbindung zwischen uns, der Welt und anderen an. Was wir Erleben im Kontakt mit den Elemente ist durch unsere Sichtweise bestimmt. Das erleben der Elemente als äußere Phänomene in Raum und Zeit ist durch unsere Gewohnheit alles als Dinge zu erleben geprägt ein Buddha würde die selben Elemente anders erleben.



### ... bald mehr ...
