---
title: 'Privacy'
date: 2018-02-22T17:01:34+07:00
image: '/services/default.png'
menu: footer
weight: 6
featured: true
draft: false
intro_image: images/undraw/undraw_signal_searching_bhpc.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


Data protection concerns us all. Here are some tips that go beyond the usual disclaimers and hopefully can be expanded in the future.
