---
title: 'Privacy notice'
date: 2020-02-22
image: '/services/default.png'
featured: true
draft: false
---



## Avoiding data traces is the best data protection –


-- that's why it's best to use the TOR network right away and disguise it your identity, it is your right to protect everyone for whom it is the only chance to protect yourself and your friends from persecution.

## TOR

In order to be able to use the TOR network, you need the TOR browser. To do this, download a software bundle to your computer from the <https://www.torproject.org/de/> and install it. You can find information on how TOR works here: <https://de.wikipedia.org/wiki/Tor_ (Netzwerk)>. Read where the safety limits are when you use TOR. Please also always keep in mind: From a technical point of view, security begins with your environment, the end device and its components as well as the operating system. You can find more information here:

* https://www.ccc.de/
* https://www.eff.org/

_or also here: _

... more translation will be here soon ...

## Unabhängiges Landeszentrum für Datenschutz Schleswig-Holstein

* https://www.datenschutzzentrum.de/


    Telefon: +49 (0) 431 988-1200

    Mo-Do von 7:00 Uhr bis 16:00 Uhr,

    Fr von 7:00 Uhr bis 15:00 Uhr

    (Kernzeit jeweils 9:00 bis 15:00 Uhr)



## Allgemeine Hinweise

Die folgenden Hinweise geben einen einfachen Überblick darüber, was mit
Ihren personenbezogenen Daten passiert, wenn Sie unsere Website
besuchen. Personenbezogene Daten sind alle Daten, mit denen Sie
persönlich identifiziert werden können. Ausführliche Informationen zum
Thema Datenschutz entnehmen Sie unserer unter diesem Text aufgeführten
Datenschutzerklärung.

### Datenerfassung auf unserer Website

**Wer ist verantwortlich für die Datenerfassung auf dieser Website?**

Die Datenverarbeitung auf dieser Website erfolgt durch den
Websitebetreiber.

{{% addr/impress  %}}

Dessen Kontaktdaten können Sie auch dem Impressum dieser
Website entnehmen können.

**Wie erfassen wir Ihre Daten?**

Ihre Daten werden zum einen dadurch erhoben, dass Sie uns diese
mitteilen. Hierbei kann es sich z.B. um Daten handeln, die Sie in ein
Kontaktformular eingeben.

Andere Daten werden automatisch beim Besuch der Website durch unsere
IT-Systeme erfasst. Das sind vor allem technische Daten (z.B.
Internetbrowser, Betriebssystem oder Uhrzeit des Seitenaufrufs). Die
Erfassung dieser Daten erfolgt automatisch, sobald Sie unsere Website
betreten.

**Wofür nutzen wir Ihre Daten?**

Ein Teil der Daten wird erhoben, um eine fehlerfreie Bereitstellung der
Website zu gewährleisten. Andere Daten können zur Analyse Ihres
Nutzerverhaltens verwendet werden.

**Welche Rechte haben Sie bezüglich Ihrer Daten?**

Sie haben jederzeit das Recht unentgeltlich Auskunft über Herkunft,
Empfänger und Zweck Ihrer gespeicherten personenbezogenen Daten zu
erhalten. Sie haben außerdem ein Recht, die Berichtigung, Sperrung oder
Löschung dieser Daten zu verlangen. Hierzu sowie zu weiteren Fragen zum
Thema Datenschutz können Sie sich jederzeit unter der im Impressum
angegebenen Adresse an uns wenden. Des Weiteren steht Ihnen ein
Beschwerderecht bei der zuständigen Aufsichtsbehörde zu.

### Analyse-Tools und Tools von Drittanbietern

Beim Besuch unserer Website kann Ihr Surf-Verhalten statistisch
ausgewertet werden. Das geschieht vor allem mit Cookies und mit
sogenannten Analyseprogrammen. Die Analyse Ihres Surf-Verhaltens erfolgt
in der Regel anonym; das Surf-Verhalten kann nicht zu Ihnen
zurückverfolgt werden. Sie können dieser Analyse widersprechen oder sie
durch die Nichtbenutzung bestimmter Tools verhindern. Detaillierte
Informationen dazu finden Sie in der folgenden Datenschutzerklärung.

Sie können dieser Analyse widersprechen. Über die
Widerspruchsmöglichkeiten werden wir Sie in dieser Datenschutzerklärung
informieren.

## Allgemeine Hinweise und Pflichtinformationen


### Datenschutz

Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten
sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und
entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser
Datenschutzerklärung.

Wenn Sie diese Website benutzen, werden verschiedene personenbezogene
Daten erhoben. Personenbezogene Daten sind Daten, mit denen Sie
persönlich identifiziert werden können. Die vorliegende
Datenschutzerklärung erläutert, welche Daten wir erheben und wofür wir
sie nutzen. Sie erläutert auch, wie und zu welchem Zweck das geschieht.

Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei
der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein
lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht
möglich.

### Hinweis zur verantwortlichen Stelle

Die verantwortliche Stelle für die Datenverarbeitung auf dieser Website
ist:


{{% addr/impress  %}}


Verantwortliche Stelle ist die natürliche oder juristische Person, die
allein oder gemeinsam mit anderen über die Zwecke und Mittel der
Verarbeitung von personenbezogenen Daten (z.B. Namen, E-Mail-Adressen o.
Ä.) entscheidet.

### Widerruf Ihrer Einwilligung zur Datenverarbeitung

Viele Datenverarbeitungsvorgänge sind nur mit Ihrer ausdrücklichen
Einwilligung möglich. Sie können eine bereits erteilte Einwilligung
jederzeit widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an
uns. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung
bleibt vom Widerruf unberührt.

### Beschwerderecht bei der zuständigen Aufsichtsbehörde

Im Falle datenschutzrechtlicher Verstöße steht dem Betroffenen ein
Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Zuständige
Aufsichtsbehörde in datenschutzrechtlichen Fragen ist der
Landesdatenschutzbeauftragte des Bundeslandes, in dem unser Unternehmen
seinen Sitz hat. Eine Liste der Datenschutzbeauftragten sowie deren
Kontaktdaten können folgendem Link entnommen
werden: <https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html>.

### Recht auf Datenübertragbarkeit

Sie haben das Recht, Daten, die wir auf Grundlage Ihrer Einwilligung
oder in Erfüllung eines Vertrags automatisiert verarbeiten, an sich oder
an einen Dritten in einem gängigen, maschinenlesbaren Format aushändigen
zu lassen. Sofern Sie die direkte Übertragung der Daten an einen anderen
Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch
machbar ist.

### Auskunft, Sperrung, Löschung

Sie haben im Rahmen der geltenden gesetzlichen Bestimmungen jederzeit
das Recht auf unentgeltliche Auskunft über Ihre gespeicherten
personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der
Datenverarbeitung und ggf. ein Recht auf Berichtigung, Sperrung oder
Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema
personenbezogene Daten können Sie sich jederzeit unter der im Impressum
angegebenen Adresse an uns wenden.

### Widerspruch gegen Werbe-Mails

Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten
Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter
Werbung und Informationsmaterialien wird hiermit widersprochen. Die
Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im
Falle der unverlangten Zusendung von Werbeinformationen, etwa durch
Spam-E-Mails, vor.

## Datenerfassung auf unserer Website

### Cookies

Die Internetseiten verwenden teilweise so genannte Cookies. Cookies
richten auf Ihrem Rechner keinen Schaden an und enthalten keine Viren.
Cookies dienen dazu, unser Angebot nutzerfreundlicher, effektiver und
sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem
Rechner abgelegt werden und die Ihr Browser speichert.

Die meisten der von uns verwendeten Cookies sind so genannte
"Session-Cookies". Sie werden nach Ende Ihres Besuchs automatisch
gelöscht. Andere Cookies bleiben auf Ihrem Endgerät gespeichert bis Sie
diese löschen. Diese Cookies ermöglichen es uns, Ihren Browser beim
nächsten Besuch wiederzuerkennen.

Sie können Ihren Browser so einstellen, dass Sie über das Setzen von
Cookies informiert werden und Cookies nur im Einzelfall erlauben, die
Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie
das automatische Löschen der Cookies beim Schließen des Browser
aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität
dieser Website eingeschränkt sein.

Cookies, die zur Durchführung des elektronischen Kommunikationsvorgangs
oder zur Bereitstellung bestimmter, von Ihnen erwünschter Funktionen
(z.B. Warenkorbfunktion) erforderlich sind, werden auf Grundlage von
Art. 6 Abs. 1 lit. f DSGVO gespeichert. Der Websitebetreiber hat ein
berechtigtes Interesse an der Speicherung von Cookies zur technisch
fehlerfreien und optimierten Bereitstellung seiner Dienste. Soweit
andere Cookies (z.B. Cookies zur Analyse Ihres Surfverhaltens)
gespeichert werden, werden diese in dieser Datenschutzerklärung
gesondert behandelt.

### Server-Log-Dateien

Der Provider der Seiten erhebt und speichert automatisch Informationen
in so genannten Server-Log-Dateien, die Ihr Browser automatisch an uns
übermittelt. Dies sind:

-   Browsertyp und Browserversion
-   verwendetes Betriebssystem
-   Referrer URL
-   Hostname des zugreifenden Rechners
-   Uhrzeit der Serveranfrage
-   IP-Adresse

Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht
vorgenommen.

Grundlage für die Datenverarbeitung ist Art. 6 Abs. 1 lit. f DSGVO, der
die Verarbeitung von Daten zur Erfüllung eines Vertrags oder
vorvertraglicher Maßnahmen gestattet.

## Analyse Tools und Werbung

### Google-Analytics und Google-Signale

Diese Website nutzt Funktionen des Webanalysedienstes Google Analytics.
Anbieter ist die Google Inc., 1600 Amphitheatre Parkway, Mountain View,
CA 94043, USA.

Google Analytics verwendet so genannte „Cookies". Das sind Textdateien,
die auf Ihrem Computer gespeichert werden und die eine Analyse der
Benutzung der Website durch Sie ermöglichen. Die durch den Cookie
erzeugten Informationen über Ihre Benutzung dieser Website werden in der
Regel an einen Server von Google in den USA übertragen und dort
gespeichert.

Die Speicherung von Google-Analytics-Cookies erfolgt auf Grundlage von
Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes
Interesse an der Analyse des Nutzerverhaltens, um sowohl sein Webangebot
als auch seine Werbung zu optimieren.

**IP Anonymisierung**

Wir haben auf dieser Website die Funktion IP-Anonymisierung aktiviert.
Dadurch wird Ihre IP-Adresse von Google innerhalb von Mitgliedstaaten
der Europäischen Union oder in anderen Vertragsstaaten des Abkommens
über den Europäischen Wirtschaftsraum vor der Übermittlung in die USA
gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server
von Google in den USA übertragen und dort gekürzt. Im Auftrag des
Betreibers dieser Website wird Google diese Informationen benutzen, um
Ihre Nutzung der Website auszuwerten, um Reports über die
Websiteaktivitäten zusammenzustellen und um weitere mit der
Websitenutzung und der Internetnutzung verbundene Dienstleistungen
gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google
Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit
anderen Daten von Google zusammengeführt.

**Browser Plugin**

Sie können die Speicherung der Cookies durch eine entsprechende
Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch
darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche
Funktionen dieser Website vollumfänglich werden nutzen können. Sie
können darüber hinaus die Erfassung der durch den Cookie erzeugten und
auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an
Google sowie die Verarbeitung dieser Daten durch Google verhindern,
indem Sie das unter dem folgenden Link verfügbare Browser-Plugin
herunterladen und
installieren: <https://tools.google.com/dlpage/gaoptout?hl=de>.

**Widerspruch gegen Datenerfassung**

Sie können die Erfassung Ihrer Daten durch Google Analytics verhindern,
indem Sie auf folgenden Link klicken.

* Für Chrome: https://chrome.google.com/webstore/detail/google-analytics-opt-out/fllaojicojecljbmefodhfapmkghcbnh?hl=de
* Für Firefox:  https://addons.mozilla.org/de/firefox/addon/google-analytics-opt-out/

Es wird ein Opt-Out-Cookie gesetzt, der die Erfassung Ihrer Daten bei zukünftigen Besuchen dieser Website verhindert.

Mehr Informationen zum Umgang mit Nutzerdaten bei Google Analytics
finden Sie in der Datenschutzerklärung von Google: <https://support.google.com/analytics/answer/6004245?hl=de>. Zugang zu Ihren Aktivitäten finden hier: <https://myactivity.google.com/myactivity>

**Auftragsdatenverarbeitung**

Wir haben mit Google einen Vertrag zur Auftragsdatenverarbeitung
abgeschlossen und setzen die strengen Vorgaben der deutschen
Datenschutzbehörden bei der Nutzung von Google Analytics vollständig um.

**Demografische Merkmale bei Google Analytics**

Diese Website nutzt die Funktion "demografische Merkmale" von Google
Analytics. Dadurch können Berichte erstellt werden, die Aussagen zu
Alter, Geschlecht und Interessen der Seitenbesucher enthalten. Diese
Daten stammen aus interessenbezogener Werbung von Google sowie aus
Besucherdaten von Drittanbietern. Diese Daten können keiner bestimmten
Person zugeordnet werden. Sie können diese Funktion jederzeit über die
Anzeigeneinstellungen in Ihrem Google-Konto deaktivieren oder die
Erfassung Ihrer Daten durch Google Analytics wie im Punkt "Widerspruch
gegen Datenerfassung" dargestellt generell untersagen.

## Plugins und Tools

### YouTube

Unsere Website nutzt Plugins der von Google betriebenen Seite YouTube.
Betreiber der Seiten ist die YouTube, LLC, 901 Cherry Ave., San Bruno,
CA 94066, USA.

Wenn Sie eine unserer mit einem YouTube-Plugin ausgestatteten Seiten
besuchen, wird eine Verbindung zu den Servern von YouTube hergestellt.
Dabei wird dem YouTube-Server mitgeteilt, welche unserer Seiten Sie
besucht haben.

Wenn Sie in Ihrem YouTube-Account eingeloggt sind, ermöglichen Sie
YouTube, Ihr Surfverhalten direkt Ihrem persönlichen Profil zuzuordnen.
Dies können Sie verhindern, indem Sie sich aus Ihrem YouTube-Account
ausloggen.

Die Nutzung von YouTube erfolgt im Interesse einer ansprechenden
Darstellung unserer Online-Angebote. Dies stellt ein berechtigtes
Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar.

Weitere Informationen zum Umgang mit Nutzerdaten finden Sie in der
Datenschutzerklärung von YouTube
unter: <https://www.google.de/intl/de/policies/privacy>.

### Vimeo

Unsere Website nutzt Plugins des Videoportals Vimeo. Anbieter ist die
Vimeo Inc., 555 West 18th Street, New York, New York 10011, USA.

Wenn Sie eine unserer mit einem Vimeo-Plugin ausgestatteten Seiten
besuchen, wird eine Verbindung zu den Servern von Vimeo hergestellt.
Dabei wird dem Vimeo-Server mitgeteilt, welche unserer Seiten Sie
besucht haben. Zudem erlangt Vimeo Ihre IP-Adresse. Dies gilt auch dann,
wenn Sie nicht bei Vimeo eingeloggt sind oder keinen Account bei Vimeo
besitzen. Die von Vimeo erfassten Informationen werden an den
Vimeo-Server in den USA übermittelt.

Wenn Sie in Ihrem Vimeo-Account eingeloggt sind, ermöglichen Sie Vimeo,
Ihr Surfverhalten direkt Ihrem persönlichen Profil zuzuordnen. Dies
können Sie verhindern, indem Sie sich aus Ihrem Vimeo-Account ausloggen.

Weitere Informationen zum Umgang mit Nutzerdaten finden Sie in der
Datenschutzerklärung von Vimeo unter: <https://vimeo.com/privacy>.

### Google Web Fonts

Diese Seite nutzt zur einheitlichen Darstellung von Schriftarten so
genannte Web Fonts, die von Google bereitgestellt werden. Beim Aufruf
einer Seite lädt Ihr Browser die benötigten Web Fonts in ihren
Browsercache, um Texte und Schriftarten korrekt anzuzeigen.

Zu diesem Zweck muss der von Ihnen verwendete Browser Verbindung zu den
Servern von Google aufnehmen. Hierdurch erlangt Google Kenntnis darüber,
dass über Ihre IP-Adresse unsere Website aufgerufen wurde. Die Nutzung
von Google Web Fonts erfolgt im Interesse einer einheitlichen und
ansprechenden Darstellung unserer Online-Angebote. Dies stellt ein
berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar.

Wenn Ihr Browser Web Fonts nicht unterstützt, wird eine Standardschrift
von Ihrem Computer genutzt.

Weitere Informationen zu Google Web Fonts finden Sie
unter <https://developers.google.com/fonts/faq> und in der
Datenschutzerklärung von
Google: <https://www.google.com/policies/privacy/>.

### Google Maps

Diese Seite nutzt über eine API den Kartendienst Google Maps. Anbieter
ist die Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043,
USA.

Zur Nutzung der Funktionen von Google Maps ist es notwendig, Ihre IP
Adresse zu speichern. Diese Informationen werden in der Regel an einen
Server von Google in den USA übertragen und dort gespeichert. Der
Anbieter dieser Seite hat keinen Einfluss auf diese Datenübertragung.

Die Nutzung von Google Maps erfolgt im Interesse einer ansprechenden
Darstellung unserer Online-Angebote und an einer leichten Auffindbarkeit
der von uns auf der Website angegebenen Orte. Dies stellt ein
berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar.

Mehr Informationen zum Umgang mit Nutzerdaten finden Sie in der
Datenschutzerklärung von
Google: <https://www.google.de/intl/de/policies/privacy/>.

## Änderung unserer Datenschutzerklärung

Um zu gewährleisten, dass unsere Datenschutzerklärung stets den
aktuellen gesetzlichen Vorgaben entspricht, behalten wir uns jederzeit
Änderungen vor. Das gilt auch für den Fall, dass die
Datenschutzerklärung aufgrund neuer oder überarbeiteter Leistungen, zum
Beispiel neuer Serviceleistungen, angepasst werden muss. Die neue
Datenschutzerklärung greift dann bei Ihrem nächsten Besuch auf unserem
Angebot.

# Danke für Ihre Aufmerksamkeit
Wenn Sie das bis hierher Aufmerksam gelesen haben, dann gehören Sie einer Minderheit an und sollten erneut den Absatz zur Datenvermeidung lesen. Den gerade der Schutz von Minderheiten ist mit TOR möglich :-))  .
