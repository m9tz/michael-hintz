---
title: 'Erst Gespräch'
discription:
date: 2020-04-01
menu:
summary: '
In einem ersten Gespräch geht darum sich kennen zu lernen. Danach können weiter Probesitzungen oder eine Bedenkzeit vereinbart werden. Die Beziehung kann natürlich weiter entwicket werden aber ein erter persönlicher Eindruck ist eine echte Entscheidungsgrundlage für das weitere Vorgehen.'
categories: [one]
tags: [two,three,four]
image:
draft: true
featured: true
weight: 3
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Im erstes Gespräch


In einem ersten Gespräch geht darum sich kennen zu lernen. Danach können weiter Probesitzungen oder eine Bedenkzeit vereinbart werden. Die Beziehung kann natürlich weiter entwickelt werden aber ein erster persönlicher Eindruck ist eine echte Entscheidungsgrundlage für das weitere Vorgehen.

geht es auch um die Vorgeschichte und um die eigenen Erwartungen.

## ...  bald mehr ...
