---
title: 'Psychiater'
discription:
date: 2020-03-12
menu:
summary:
categories: [one]
tags: [two,three,four]
image:
draft: true
featured:
weight: 5
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Psychiater

Auch ein Psychiater kann mit der Kasse abrechnen er ist ein Facharzt und hat Medizin studiert z. B. mit der Fachrichtung Psychotherapie. Aber auch andere Fachärzte bieten Psychotherapie an z. B. der Facharzt für Kinder- und Jugendpsychiatrie und Psychotherapie. Sie merken schon das der Bereich etwas unübersichtlich ist. Ob jemand Erfolge als Therapeut erzielt hängt nur zum Teil von seiner Ausbildung ab, sondern meiner Meinung nach von seiner Persönlichkeit und dem Kontakt der zwischen Ihnen und dem Therapeuten entsteht.
