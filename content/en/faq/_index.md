---
title: 'Facts'
discription:
date: 2020-05-01
menu: main
summary:
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 4
weight_front:
weight_print:
intro_image: images/undraw/undraw_personal_text_vkd8.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# short answers to frequently asked questions

If you have only a few ideas about which forms of therapy are available and which ones might suit you and your change wishes, I would like to give you a rough overview here. There is also some knowledge about job descriptions and designations. How you can get the costs reimbursed will be found here soon.
