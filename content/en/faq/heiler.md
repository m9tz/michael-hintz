﻿---
title: 'HeilerInnen'
discription:
date: 2020-05-15
menu:
summary:
categories: [one]
tags: [two,three,four]
image:
draft: true
featured:
weight: 7
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# HeilerInnen

Eine neue Berufsbezeichnung ist der Heiler oder Heilerin diese/dieser macht z. B. Fernheilung, Hand auflegen oder Gebetsheilung. Das hört zunächst sehr esoterisch an. Ist es aber auch, wer hat nicht schon mal von einer erfolgreichen Behandlung von Warzen durch Besprechen gehört! Das Feld ist vielfältig und greif auch in die Psyche ein. Hier ist zu beachten das der Heiler nicht wie der Heilpraktiker eine gute Grundkenntnis des medizinisch machbaren, notwendigem und ggf. meldepflichtigen Krankheitswissens hat. Eine nicht erkannte ansteckende Krankheit kann weitreichende Folgen haben. Die eigene Verantwortung sollte niemals an der Garderobe abgegeben werden weder beim Arzt noch beim Heiler.
