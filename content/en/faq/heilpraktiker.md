---
title: 'Heilpraktiker'
discription:
date: 2020-02-11
menu:
summary: '
Als Heilpraktiker bin ich zwar vor allem im Bereich der Psychotherapie tätig aber einen Blick auf all die anderen Felder eines gesunden und ausgewogenen Lebens zu haben unterscheidet den Heilpraktiker von anderen Heilberufen.'
categories: [one]
tags: [two,three,four]
image:
draft: true
featured: true
weight: 1
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# Heilpraktiker

In Deutschland gibt es schon sehr lange den Beruf des Heilpraktikers auch dieser kann eine Therapiemethode erlernt haben. Diese Methode kann eine psychotherapeutische sein. Seit vielen Jahren gibt es den speziellen Heilpraktiker für Psychotherapie, dieser darf ausschließlich psychotherapeutisch tätig sein, also nicht auch zusätzlich Heilpflanzen oder Massage anbieten. Bei allen Heilpraktikern ist es wichtig zu wissen, dass die Berufsbezeichnung als solche nichts über die Qualität dieser Person als Anwender der Methode Psychotherapie aussagt. Die Zulassung als Heilpraktiker bestätigt zunächst nur das dieser seine gesetzlichen Grenzen kennt und für seine Handlungen haftet, weil er weiß was er medizinisch nicht tun darf. Wenn er oder Sie jedoch nachweisen kann, dass er eine bestimmte Methode beherrscht darf er oder sie sehr viel tun. Heilpraktiker können einige Leistungen mit einigen Kassen abrechnen aber in der Regel keine psychotherapeutischen.

Der Beruf des Heilpraktikers ist sehr beliebt, weil er anders als der des Arztes nicht an so enge Vorstellungen von Wissenschaftlichkeit gebunden ist. Das betrachte ich persönlich als Vorteil. Nicht nur in der Psychotherapie ist unsere, seit der Aufklärung doch recht Materialistische abendländische Vorstellung von der Welt der Biologie, des Lebens und der Psyche nicht immer ausreichend ist um zu heilen. Damit möchte ich in keiner Weise die riesigen Errungenschaften dieses Ansatzes klein reden. Gerade die Psyche ist es aber die besonders in den alten Hochkulturen Asiens schon seit Jahrtausenden erforscht wird. Viele dieser alten Techniken und Ansichten finden heute Einzug in „moderne“ Therapiemethoden. Bei der Integration dieser am Anfang oft als unwissenschaftlich betrachteten Verfahren leisten die Heilpraktiker einen wichtigen Beitrag.
