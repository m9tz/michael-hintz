---
title: 'Psychologe'
discription:
date: 2020-01-20
menu:
summary:
categories: [one]
tags: [two,three,four]
image:
draft: true
featured:
weight: 6
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Psychologe

Ein Psychologe ist jemand der Psychologie studiert hat, das ist zunächst eine recht wissenschaftliche Ausbildung. Dieser Mensch kann dann noch eine mehrjährige Ausbildung in einer oder mehreren Psychotherapiemethoden absolviert haben er oder sie sich psychologischer Psychotherapeut nennen. Dieser Titel ist geschützt und wird meist als Freier Beruf ausgeübt. Wenn dieser Mensch dann auch noch zufällig oder beabsichtigt die Methoden erlernt hat die deutschen Krankenversicherungen für hilfreich halten, dann könnte dieser Mensch ihnen eine mit der Krankenkasse abrechenbare Psychotherapie anbieten, vorausgesetzt er oder sie hat auch eine Kassenzulassung und freie Kapazitäten in der Praxis.
