---
title: 'Home'
date: 2020-01-06
Description: "Michael Hintz, Heilpraktiker: Psychothrapie, Coaching und Hakomi"
intro_image: images/logo.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# Psychotherapy and Coaching

## Michael Hintz -- alternative practitioner since 1994


Our feeling of happiness and contentment depends on many levels of our being there. Physical health and external living conditions, relationships with other people but also our natural spirituality need its space. Finding a way out of a painful sensation to experience more vitality and contentment is often tedious and difficult without help. I see myself as a companion for your very personal path.
