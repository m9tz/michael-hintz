---
title: 'Cosima Hagyó'
date: 2020-01-19
draft: false
image: 'images/team/cosima.jpg'
jobtitle: 'Dipl. Sozialpädagogin & Therapeutin'
email: 'cosima@hagyo.de'
wwwurl: 'https://www.hagyo.de'
weight: 4
---

Syst. Paar- und Familientherapeutin

* Traumatherapie "PITT Kids"
* EgoState/EMDR/Hypnotherapie
* Verfahrensbeistand/Umgangs-und Ergänzungspflegschaft/Vormund
* Lösungsorientierte Sachverständige
