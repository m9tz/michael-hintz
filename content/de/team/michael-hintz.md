---
title: 'Michael Hintz'
date: 2020-01-19
draft: false
image: 'images/team/ich2018.jpg'
jobtitle: 'Coach & Psychotherapeut (HeilprG)'
email: 'mhi@posteo.de'
wwwurl: 'https://www.michael-hintz.de'
weight: 2
---

Als Heilpraktiker, Paar- und Familientherapeut und Hakomi-Therapeut biete ich Ihnen
psychotherapeutische Hilfe an. Meine ganzheitliche Sicht auf die Entwicklung und
Heilung schließt aber auch die Bereiche "Ernährung", "Bewegung" und "soziales Umfeld" mit ein.

Meine primäre Arbeitsweise ist die Hakomi-Therapie, sie ist eine "Körperorientierte Psychotherapie".
Das bedeutet, dass ich Ihnen neben dem reinen Gespräch auch viele Möglichkeiten biete, über
den Körper Zugang zu den vielfältigen Eindrücken zu bekommen, die Sie als ganzen Menschen ausmachen.
