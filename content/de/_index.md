---
title: 'Home'
date: 2023-08-07
Description: "Michael Hintz, Heilpraktiker: Psychothrapie, Coaching und Hakomi"
intro_image: images/logo.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# Psychotherapie und Coaching

## Michael Hintz  -- Heilpraktiker seit 1994


Unsere Empfindung von Glück und Zufriedenheit hängt von vielen Ebenen unseres Daseins ab.
Die körperliche Gesundheit und die äußeren Lebensumstände, die Beziehungen zu anderen
Menschen aber auch unsere natürliche Spiritualität braucht seinen Raum. Einen Weg aus
einer leidvollen Empfindung heraus zu finden, um mehr Lebendigkeit und Zufriedenheit zu
erleben, ist ohne Hilfe oft langwierig und schwierig. Ich verstehe mich als ein Begleiter
für ihren ganz persönlichen Weg.
