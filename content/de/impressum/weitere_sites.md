---
title: 'weitere websites'
date: 2021-11-18
description: 'Im Sinne des Pressegesetztes verantwortlich ...'
menu:
weight: 5
image: '/services/default.png'
featured: true
draft: false
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

## weitere Websites:


* [Hauptseite Kopie](https://www.michael-hintz.de)

* [Hauptseite](https://www.mhi.biz)

* [Xing Profile](https://www.xing.com/profile/Michael_Hintz3/)

* [Hakomi Therapeutenliste](https://www.hakomi.de/87/hakomi/therapeutenliste)
