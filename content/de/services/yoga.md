---
title: 'Yoga und Therapie'
discription: ''
date: 2021-01-01
menu:
summary: '
Yoga ist heute Livestyle und in aller Munde.
Yoga kann zur Erleuchtung führen und hat viele positive gesundheitliche Aspekte.'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 2
weight_front:
weight_print:
# intro_image: images/undraw/undraw_yoga_248n.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Yoga und Therapie

Yoga ist heute Livestyle und in aller Munde.
Yoga kann zur Erleuchtung führen und viele positive gesundheitliche Aspekte.

## Yoga, wie ich es verstehe

1990 hieß mein erster Yoga-Lehrer in Lübeck "Amnuai" und kam aus Thailand. Er folgte einer
buddhistischen Yogalinie. Seit vielen Jahren folge ich nicht nur in Sachen "Yoga" einer tibetischen Tradition, dem
[Yantra-Yoga](http://www.yantrayoga.net/), die zuerst von
[Chögyal Namkhai Norbu](http://dodjungling.de/choegyal-namkhai-norbu/) im Westen gelehrt wurde.

Yoga ermöglicht uns den Atem bewusst zu lenken und damit auch das Bewusstsein. Der Atem ist die
Schnittstelle zwischen physischem Körper und dem energetischem Geschehen in uns. Diese beiden
Ebenen sind es, die auch in der Psychotherapie angesprochen werden, dort oft durch Sprache und
inneres Erleben. In körperorientierten Methoden wie dem "Hakomi" aber auch direkt über den Körper kann das erlebt werden.
Yoga ist weder mit dem Hinduismus noch mit dem Buddhismus verbunden, sondern ist ein eigenständiger
Weg und als solcher auch auch als Lifestyle lehrbar. Es gibt viele Yoga-Traditionen,
auch recht exotisch anmutende wie [Tensegrity®](https://www.youtube.com/watch?v=LukImQbfTCY) oder
[Tumo/Innere Hitze](https://www.youtube.com/watch?v=BESrdlf-cPg), allen gemeinsam ist aber, dass
es nicht vorrangig um Gesundheit geht sondern um Erkenntnis.

## Yoga als Selbsterfahrung

Jede Erfahrung, die bewusst wird, ändert das Bewusstsein und gibt ein positives Feedback, das dazu
führt, die natürliche innere Achtsamkeit weiter zu praktizieren. Daher geht es bei der sogenannten
"Yogatherapie" oder "Yoga als Selbsterfahrung" nicht um den körperlichen Aspekt der Yogaübung. Es
geht um die Bewusstwerdung der eigenen Wahrnehmung von inneren und äußeren Eindrücken und letztlich
um die Auflösung dieser festen anmutenden Trennung. Der körperliche Aspekt ist unbestritten und
hat als solches natürlich seinen Wert, sollte jedoch nicht als alleiniger Zweck der Motivation dienen
 - dann ist Gymnastik und Sport sicherlich sinnvoller.

## Ausgleichen der Elemente durch Yoga

Die Vorstellung von dem Gleichgewicht der Elemente in unserem Körper ist ein grundlegendes Prinzip
fast aller alten Medizintraditionen. Für die Vorstellung, dass Körper und Geist einander beeinflussen, gilt das ebenso.
Wie Yoga in diese Wechselwirkung eingreift und auch gezielt eingesetzt wird, ist in einigen
Büchern gut beschrieben.

Hier folgt aber noch ein weiterer Aspekt der Elemente, die im Yoga auch berührt
werden können: Die im Außen auftretenden Elemente sind in Verbindung mit den inneren Elementen. Wir
kennen das alle. Ob wir uns einer sonnenreichen Umgebung oder einer windreichen Umgebung aussetzen, hat für uns eine
 völlig unterschiedliche Auswirkung. Aber auch der durch Visualisierung generierte Kontakt zu einem Element
ändert unser Erleben und wirkt bis in die Psyche hinein. Viele Traditionen sehen die Ebene der Elemente
als die eigentliche Verbindung zwischen uns, der Welt und anderen an. Was wir im Kontakt
mit den Elementen erleben, ist durch unsere Sichtweise bestimmt. Das Erleben der Elemente als äußere Phänomene in Raum und
Zeit ist durch unsere Gewohnheit, alles als Dinge zu erleben, geprägt, ein Buddha würde die selben
Elemente anders erleben.
