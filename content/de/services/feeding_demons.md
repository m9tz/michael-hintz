---
title: 'Dämonen füttern'
discription:
date: 2020-11-01
menu:
summary: '
Mit Dämonen ist das eigene Erleben von Situation und Erinnerungen gemeint. Wenn wir dem Raum geben und feststellen
was sie uns sagen wollen können wir sie befrieden und sie können zu Verbündeten werden.'
categories: [one]
tags: [two,three,four]
image:
draft: true
featured: true
weight: 5
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# Dänonen füttern

Eine alte Methode weiterentwickelt. Noch gibt es nur einen [PDF Entwurf](/doc/Feeding_Demons.pdf) von mir und natürlich das
[Buch](https://www.randomhouse.de/leseprobe/Den-Daemonen-Nahrung-geben/leseprobe_9783442338306.pdf) von
[Lama Tsultrim Allione](https://www.taramandala.org/introduction/lama-tsultrim-allione/books/#feeding)
