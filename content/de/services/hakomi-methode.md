---
title: 'Achsamkeitszentrierte Körperpsychotherapie'
discription:
date: 2022-07-13
menu:
summary: '
Unser Körper und die eigene Psyche stehen in enger Wechselwirkung mit einander.
Viele Erlebnisse sind sogar nur in Verbindung mit einem körperlichen Erinnern abrufbar.
Innere Achtsamkeit ist der Zugang zu diesem System und ist für jeden nutzbar.'
categories: [one]
tags: [two,three,four]
image: images/undraw/undraw_abstract_x68e.svg
draft: false
featured: true
weight: 5
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

## HAKOMI - Achtsamkeitszentrierte Köperpsychotherapie

Während der letzten 50 Jahre hat sich die klassische Psychotherapie auf verschiedene Weisen verändert.
Wichtig ist zum Beispiel der Wechsel vom Reden über eine Erfahrung zum Erleben einer Erfahrung in der
gegenwärtigen therapeutischen Situation. Anschließend geht es zur Untersuchung, wie wir jene Erfahrung selbst
gestalten und organisieren.

{{< img-l s="/images/Hakomi-Logo.png" w="90px" >}}

Die HAKOMI®-Methode berücksichtigt in ihrem körperbezogenen Ansatz neben der tiefenpsychologischen und
systemischen Perspektive auch transpersonale Aspekte in der psychotherapeutischen Arbeit. Hieraus ergibt sich
eine einzigartige Möglichkeit, konflikt- und lösungszentriertes, prozesshaftes und bewusstseinsorientiertes
Vorgehen miteinander zu verbinden.


### Hakomi ist Körperorientiert

Der Körper ist eines der besten Mittel, die Selbstorganisation eines Menschen im gegenwärtigen Erleben zu
untersuchen und zu verstehen. Wie wir uns als Ganzes in unserem Verhalten, in Gefühlen, Erinnerungen und
Sichtweisen organisieren, einschließlich all dem, was uns nicht bewusst ist - der Körper spiegelt es wider.
Darum ist es wertvoll, seine Sprache durch Achtsamkeit wahrnehmen zu können und sie zu verstehen. So
werden viele wichtige Kernüberzeugungen augenblicklich aufrufbar und erlebbar, wenn wir mit der physischen,
sichtbaren Ebene unseres Seins arbeiten.

Wir setzen Körperbewusstsein ein, um die Selbstorganisation eines Menschen auf eine leichte Art im
gegenwärtigen Erleben untersuchen zu können. Mit körperlichen Interventionen, die präzise und achtsam
durchgeführt werden,
lenken wir die Aufmerksamkeit und eröffnen neue Wege des Erlebens.

### Gewaltlosigkeit ist eine Geisteshaltung

Einer der wesentlichen Beiträge der HAKOMI®-Methode liegt im Umgang mit der Abwehr. Genau beschriebene
therapeutische Haltungen und neu entwickelte Techniken gestalten diese Arbeit. Menschen verfügen über
eine Reihe von Mechanismen, um Einflüsse von außen abzuwehren und die eigene Integrität zu bewahren.

Es ist erschöpfend und schwierig, sich mit diesen Mechanismen auf ein Ringen einzulassen. Vieles geht
leichter und schneller, wenn wir beispielsweise die Abwehr unterstützen und sie somit der Beobachtung zugänglich machen.

Mit einer gewaltlosen Haltung laden wir das Unbewusste zur Kooperation ein, denn das Unbewusste bestimmt,
was möglich ist in einer Sitzung, und was nicht. Erst wenn es sich in der therapeutischen Beziehung nicht
bedroht fühlt, wird es die empfindlichsten Informationen freigeben.

### Mit Worten

Aus unserer Sicht wird die Qualität der Selbstorganisation eines Menschen durch den Fluss von Informationen
bestimmt. Wie verschiedene Anteile einer Person zusammenarbeiten, hängt davon ab, was sie voneinander
und über die Außenwelt wissen.

Interne Modelle der Wirklichkeit eröffnen und begrenzen die Verhaltens- und Erlebnismöglichkeiten. Worte
kennzeichnen und bewegen die symbolischen Ebenen, auf denen diese Art von Informationen gespeichert und
verändert werden kann. Worte sind auch eine wichtige Art, wie ein:e Therapeut:in ständig mit dem inneren
Erleben ihres/ihrer Klient:in in Verbindung bleiben kann und so Sorge trägt, dass sie sich nicht in
verschiedenen Welten befinden, sondern wirklich zusammenarbeiten.

### Tiefenpsychologisch

Im Zentrum unserer Arbeit steht die individuelle Struktur der Persönlichkeit eines Menschen. Das heißt,
die Art und Weise, wie innere ungelöste Konflikte und die damit einhergehenden unbewussten Anschauungen
sich z.B. auf der Körperebene und in der Beziehungsgestaltung ausdrücken und die Entfaltung des individuellen
Potenzials begrenzen.

In Übereinstimmung mit den theoretischen Konzepten der Psychoanalyse und der tiefenpsychologisch fundierten
Therapie bin ich überzeugt, dass sich diese unbewussten Anschauungen wesentlich im Kontext früher
Beziehungserfahrungen formen. Wirkliche Veränderung ist der Prozess der erfahrungsorientierten
Bewusstmachung sowie die Ermöglichung einer neuen, alternativen Erfahrung des Erlebten (missing experience).
Dies geschieht im Rahmen der therapeutischen Beziehung.

Diese Überzeugungen werden zurzeit eindrucksvoll bestätigt durch neue Ergebnisse der Neurobiologie
sowie der Säuglings- und Bindungsforschung, deren Befunde immer wieder darauf verweisen, wie frühe
bedeutsame Erfahrungen die menschliche Selbstorganisation prägen. Die achtsame Untersuchung dieser
Selbstorganisation mit dem/der Klient:in, die Bewusstmachung und emotionale Verarbeitung sowie die
Verankerung neuer Erfahrungen auf der Erlebensebene sind Kernelemente der HAKOMI®-Methode. Sie
zeichnen sich aus als tiefenpsychologisch fundierte körperorientierte Methode, die sich gerade durch das
erfahrungszentrierte Arbeiten auch mit neuesten Erkenntnissen der Therapieforschung deckt.

### Innere Achtsamkeit

Unser Alltagsbewusstsein ist kein wirksames Mittel, um tiefere Ebenen unseres Selbst zu erfahren
und zu verändern, denn unser Alltagsbewusstsein nutzt gerade diese tiefen Schichten für seine
gewohnheitsmäßige Selbstorganisation. Deshalb stehen wir beim Ringen um Veränderung oft an dem Punkt,
dass wir das Problem mit dem Verstand durchaus gut erkennen, aber die Lösungsversuche funktionieren nicht.

An dieser Stelle hilft uns innere Achtsamkeit, eine Form der Aufmerksamkeit, die sich in den
meditativen Disziplinen schon seit Jahrtausenden bewährt hat. Die langsame Schulung der inneren
Achtsamkeit baut eine immer stabiler werdende Bewusstseinsposition auf, die uns mehr und mehr
erlaubt, die Bestandteile und die Gestaltung des inneren Erlebens zu erforschen.

Zunächst bekommen wir ein besseres Gespür und Gefühl für die Fragen unseres Lebens, und schließlich
können wir zu den Grundlagen unserer Selbstorganisation Zugang finden. Dieser "rote Faden" zieht sich
oft wie in einem gewobenen Muster durch viele Bereiche unseres Lebens. Die nicht-bewussten automatischen
Steuerungsfaktoren werden allmählich ins Bewusstsein gehoben und durch eine immer umfassender werdende
"Selbstführung" organisiert. Letztlich führt der Weg der Achtsamkeit zu den Kräften der Selbstheilung
und der inneren Weisheit.
