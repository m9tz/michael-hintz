---
title: 'Therapeutische Haltung'
discription:
date: 2021-09-01
menu:
summary: '
Noch gibt es nur einen [PDF Entwurf](/doc/Therap_Haltung.pdf) von mir der im Rahmen der systemischen Ausbildung entstand.'
categories: [one]
tags: [two,three,four]
image:
draft: true
publishdate: 2021-11-27
expirydate: 2021-11-26
featured: true
weight: 9
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# Eine Annäherung an eine der entscheidenden Grundlagen

Noch gibt es nur einen [PDF Entwurf](/doc/Therap_Haltung.pdf) von mir der im Rahmen der systemischen Ausbildung entstand.
