---
title: 'Selbsterfahrung'
discription:
date: 2021-01-01
menu:
summary: ' Nach einer umfangreichen Suche nach Erkenntnis in der Jugend,
 geprägt von intensiven LSD Erfahrungen und ersten Begegnungen mit dem tibetischen Buddhismus kam es zu dem Wunsch,
 therapeutisch tätig zu werden.'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 3
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Eigene Selbsterfahrung

Nach einer umfangreichen Suche nach Erkenntnis in der Jugend,
geprägt von intensiven LSD Erfahrungen und ersten Begegnungen mit dem tibetischen Buddhismus, kam es zu dem Wunsch,
therapeutisch tätig zu werden.
Ich lernte, wie es zu dieser Zeit Mainstream war, zunächst einige Methoden von Therapeut:innenn kennen,
die damals unter anderem enge Schüler:innen von [Werner Erhard](http://wernererhard.de/biography.html) waren, der
Begründer des EST-Trainings. Aber auch Therapeut:innen aus dem Umfeld von [Osho](https://de.wikipedia.org/wiki/Osho) waren dabei.



Einen wirklich sehr intensiven und damals prägenden Eindruck hinterließ aber eine Methode
namens [Enlightenment Intensive](https://de.wikipedia.org/wiki/Enlightenment_Intensive), die
von dem Yogi und kalifornischen Kommunikationswissenschaftler [Charles Berner](http://www.charlesberner.org/)
entwickelt wurde. Diese Seminarform verbindet wie keine andere "östliche Philosophie" mit "moderner
Kommunikation" und "Psychotherapie", sie beinhaltet die intensive Auseinandersetzung mit der Frage: "Sag mir, wer du bist!",
und: "Sag mir, was ein anderer ist!".


Zeitgleich machte ich Erfahrungen mit dem [Feuerlaufen](https://de.wikipedia.org/wiki/Feuerlauf).
Meine ersten "Gehversuche" mit eigenen Gruppen drehten sich um dieses Thema.

Meine erste umfangreiche Ausbildung in einer psychotherapeutischen Methode war die "Hakomi-Methode". Dies ist eine
körperorientierte Methode, die ich in einem [eigenen Artikel](/services/hakomi/) näher erläutere.
Glücklicherweise durfte ich den Begründer "Ron Kurtz" dieser Methode sehr persönlich kennenlernen. Er hat zum Ende
seines Lebens sehr intensiv an der therapeutische Beziehung als den "eigentlich tragenden Pfeiler
jeder Therapieform" gearbeitet und dies alles in seinem letzten Buch zusammengetragen, das er vor seinem Tode nicht mehr selbst fertigstellen konnte.

Die zweite Methode, die ich studierte, ist die "Systemische Therapie", die auch eine
[eigene Darstellung](/services/systemik) hat.

Aus all diesen hier leider noch nicht vollständig beschriebenen Einflüssen hat sich eine eigene Sicht
und auch Methodik entwickelt.
