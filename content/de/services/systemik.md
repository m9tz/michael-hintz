---
title: 'Systemische Therapie'
discription:
date: 2021-11-01
menu:
summary: ' "Systemische Therapie" oder "Systemische Familientherapie" ist ein psychotherapeutisches Verfahren, dessen Schwerpunkt auf dem
sozialen Kontext psychischer Störungen liegt, insbesondere auf Interaktionen zwischen Mitgliedern der Familie
und deren sozialer Umwelt. '
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 1
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# Systemische Therapie

"Systemische Therapie" oder "Systemische Familientherapie" ist ein psychotherapeutisches Verfahren, dessen Schwerpunkt auf dem
sozialen Kontext psychischer Störungen liegt, insbesondere auf Interaktionen zwischen Mitgliedern der Familie
und deren sozialer Umwelt. In Abgrenzung zur Psychoanalyse betonen Vertreter:innen dieser Therapierichtung
die "Bedeutung impliziter Normen des Zusammenlebens für das Zustandekommen und die Überwindung
psychischer Störungen (Familienregeln)".


Allerdings berücksichtigen auch andere Therapieformen, wie z.B. die "Kognitive Kurzzeittherapie",
den "systemischen" Aspekt. Die "Systemische Therapie" unterscheidet sich nach Angaben ihrer Vertreter:innen
dadurch, dass weitere Mitglieder:innen des für den/die Patient:in relevanten sozialen Umfeldes in die Behandlung
einbezogen werden.

Seit Ende 2008 ist in Deutschland die "Systemische Therapie" als wissenschaftliches Psychotherapieverfahren anerkannt.
