---
title: 'Hakomi - eine Haltung'
discription:
date: 2021-05-11
menu:
summary: '
Unser Körper und die eigene Psyche stehen in enger Wechselwirkung mit einander.
Viele Erlebnisse sind sogar nur in Verbindung mit einem körperlichen Erinnern abrufbar.
Innere Achtsamkeit ist der Zugang zu diesem System und ist für jeden nutzbar.'
categories: [one]
tags: [two,three,four]
image: images/undraw/undraw_abstract_x68e.svg
draft: true
featured: true
weight: 6
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


## Hakomi entdecken

Mein Weg zu Hakomi war zufällig - wenn es das wirklich gibt? - vor ungefähr 28 Jahren wurde der Begründer der Methode
in das [Himalaya-Institut](https://www.himalaya-institut-ahrensburg.de) in Ahrensburg zu einem Workshop eingeladen.
Ich half damals einer Yogaschülerin [Hanne Willberg](http://hannewilberg.com/) die Eigenleistung beim Aufbau des
neuen Institutes zu erbringen. Am Tag vor dem Workshop saß ich am Mittagstisch "zufällig" neben Ron Kurtz und 24 Stunden
spähter in meinem erstem "Prozessing Workshop" mit ihm. Die Ausstrahlung und das ans magische grenzende therapeutische
Gesprühr von Ron haben mich zu tiefst berührt. Er war geleitet von sehr viel Intuition und einer tiefen Liebe zu den
Menschen den er begegnete. Ich kann auch heute noch sagen das diese Begegnung mein therapeutisches Denken und Handeln
am stärksten geprägt hat.

Die innere Achtsamkeit ist eine sehr alte Methode aber Ron Kurtz hat sie für die Psychotherapie neu umgesetzt. Lange
bevor die Innere Achsamkeit zum Mainstream in der moderne Therapie wurde fand sie Eingang in die Hakomi-Methode.
Die bekantesten Wurzeln dieser Methode der selbsterforschung finden sich beim historischem Buddha. Bis heute ist es eine der
Hauptmethoden des Hinayana Schule des Buddhismus und damit auch Grundlage für alle späteren buddhistischen Wege.


## Hakomi leben

Seit den Anfängen von Ron's therapeutischer Arbeit in den wilden frühen '70 Jahren in Califonien bis heute zieht
sich ein Thema besonders durch seine Arbeit: die therapeutische Haltung und damit die therapeutische Beziehung. Seine
letzten Jahre vor seinem Tod widmete er ganz besonders der wie er es nannte "Loving Presents". Neben allen wissenschaftlichen
Fakten und Methoden die heute der Hakomi-Methode eine festen Platz in der vielfältigen Welt der Psychtherapie Methoden
sichern ist "Loving Presents" wohl eine der Methoden die die größte Herrausforderung für Therapeuten bedeutet und zugleich
eine der wichtigsten Grundlage für eine erfolgreiche therapeuteiche Beziehung und damit Heilung darstellt.

Für viele Menschen die sich in eine Therapie begeben ist der Therapeut seit langer Zeit die ersten vertraute
Bezugsperson denen sie Zugang zu den innersten Anschauungen und Erfahrungen gewähren. Der Therapeut ist dabei in viele
Projektionen verwickelt und erfüllt viele Aufgaben aber das wichtigste ist das er für den Klienten da ist, wenn dieser
aus den tiefsten Tiefen seiner Psyche wieder im hier und jetzt ankommt und nichts mehr braucht als einen Menschen der ihn
in der neuen Welt willkommen heißt und ihm eine echte Wertschätzung entgegen bringt. An diesem Punkt ist "Loving Presents"
tausendmal wichtiger als jede "profesionelle Distanz".

Diese Haltung die echtes Mitgefühl und Liebe entgegen bringt gibt dem Klienten die Anerkennung die sie für ihre
Auseinandersetzung mit ihrer Psyche verdient haben und brauchen um eine neue Erfahrung in die Welt mitzunehmen
und eine neue Sicht auf vergangene Momente zu integriren. Die Offenheit die ein Therapeut in diesen Momenten aufbringen
muss ist leicht wenn der Therapeut nicht denkt es gibt etwas in oder an ihm zu schützen oder zu bewahren, es ist einfach
ohne eine Idee von einem Ego das zerstört werden kann aber es kann sehr anstrengend und verzehrend werden wenn die offenheit
kontroliert werden muss.

In diesem Zusammenhang ist meiner Meinung nach eine wunderbare Übung für angehende Therapeuten die ebenfalls sehr alte
Tantrisch-Buddhistische Praxis des "Chöd" eine art symbolischer Abgabe des Egos an alle Freunde und Feinde. Die Methode wurde
von der Dakini "Machig Labdrön" (1055 – 1145) ausführlich dargelegt und heute von
[Lama Tsultrim Allione](https://www.taramandala.org/introduction/lama-tsultrim-allione/recognition-story/) erneut verfeinert und
modernisiert, sie beschreibt das in ihrem Buch
["Den Dämonen Nahrung geben"](https://www.randomhouse.de/leseprobe/Den-Daemonen-Nahrung-geben/leseprobe_9783442338306.pdf) sehr praxistauglich.

## Hakomi vermitteln

Ron Kurtz war auch der Meinung, das die Hakomi-Methode auch allen anderen helfenen Berufen vermittelt werden sollte. Schon
immer teilen wohl alle Hakomi Therapeuten und viele andere psychologisch tätige dass es der Klient ist der sich ändert,
weiterentwickelt und sich heilt. Die Aufgabe des Therapeuten ist besonders beim Hakomi dem Klienten Methoden und Sichtweisen
zu vermitteln und den Raum zu schaffen in dem Selbstreflektion und Integration stattfinden kann.
