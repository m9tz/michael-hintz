---
title: 'Methoden'
discription:
date:
menu: main
summary:
categories: [one]
tags: [two,three,four]
image:
draft: false
featured:
weight: 3
weight_front:
weight_print:
intro_image: images/undraw/undraw_abstract_x68e.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---




# Lehrer:innen und Methoden

Welche Lehrer:innen und Methoden mich im Laufe der Zeit beeinflusst haben, wird
hier in einigen Artikeln beschrieben. Das Zusammentragen ist ein Prozess, der
andauert.
