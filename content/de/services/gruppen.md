---
title: 'Gruppentherapie'
discription: ''
date: 2021-10-01
menu:
summary: '
Sich selbst in einer Gruppe zu erleben, ist viel intensiver als sich alleine oder in einer Einzeltherapie
zu beobachten. Es bietet auch die Möglichkeit, die Interaktion mit anderen zu erforschen.'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 7
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Gruppentherapie

Sich selbst im Zusammensein mit Anderen zu erleben, ist für viele noch intensiver als eine Einzeltherapie.
Da hier durch die anderen Teilnehmer Themen aufgemacht werden, von denen ich spontan berührt werde und
die auch meine Themen sein können. Das Spiegeln der eigenen Persönlichkeit durch die Gruppe oder einem
einzelnen Teilnehmer kann sehr tiefe Erkenntnisse über das eigene Verhalten hervorbringen.
