---
title: 'Holz E-Boot'
date: 2022-06-09
description: ''
# menu: main
weight: 5
image: 'images/team/ich2018.jpg'
featured: true
draft: true
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# Holz-Elektro-Boot zu verkaufen

![image alt text](/images/eboot/boot_seite.jpg)


Dieses Schiff scheint ein Unikat zu sein, gefertigt aus Mahagonisperrholz.

{{< img-l s="/images/eboot/boot_vor.jpg" w="200px" >}}
{{< img-l s="/images/eboot/boot_west.jpg" w="150px" >}}

Es war wahrscheinlich lange auf der Wakenitz im Bootverleih.
Für zwei Erwachsene und zwei Kinder ist genug Platz und Auftrieb vorhanden.


Die zwei Akkus sind eine Saison bei Boot-Now gelaufen und haben ca. 80%
Kapazität der ursprünglichen 240 AH pro Akku. Ich fahre mit einem
Akku ca. 4 Stunden bei 7 Km/h. Positionslichter, Ankerlicht und Horn sind
ausreichend dimensioniert. Eine Badeleiter ist auch dabei.



![image alt text](/images/eboot/boot_inst.jpg)


Das Boot hat
neues Antifouling und ist in einem guten Zustand.



Da wir uns in Zukunft nur noch dem Segeln widmen, wollen wir uns von
diesem schönen Boot trennen. Die Übernahme des Liegeplatzes ist im gemeinsamen
Gespräch wahrscheinlich möglich.



Eine Besichtigung mit Probefahrt können wir gerne jederzeit vereinbaren. Ein Trailer
ist nicht dabei aber das Boot kann von vier starken Menschen getragen werden (ohne Akkus!).

{{< img-l s="/images/eboot/boot_innen.jpg" w="200px" >}}
{{< img-l s="/images/eboot/boot_lieg.jpg" w="200px" >}}
{{< img-l s="/images/eboot/boot_plane.jpg" w="200px" >}}
{{< img-l s="/images/eboot/boot_motor.jpg" w="200px" >}}



Der Preis ist Verhandlungssache, ich möchte das Boot in guten Händen wissen und würde
mich freuen es noch lange auf der Trave zu sehen. Mein erstes Gebot sind 3.500,- €.


Bei Fragen gerne per Telefon unter 0151-74119515

Bis bald, Michael Hintz
