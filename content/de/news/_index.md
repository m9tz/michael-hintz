---
title: 'News'
date: 2022-04-21
description: ''
menu: main
weight: 6
image: ''
featured: true
draft: true
intro_image: /images/undraw/undraw_year_2022.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# Aktuelle Hinweise und Veranstalltungen


Hier finden Sie die neusten Veranstalltungen und Gruppen und ggf.
auch Hinweise zu empfehlenswerten externen Terminen.
