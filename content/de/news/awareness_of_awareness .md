---
title: 'Meditation und Selbsterfahrung für Einsteiger'
date: 2020-05-15
description: ''
# menu: main
weight: 5
image: ''
featured: true
draft: true
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

Demnächst startet eine neue Gruppe. Sie wird fortlaufend sein und soll einen Einstieg in einige Methoden der Selbsterfahrung bieten, die hier zum Teil schon beschrieben sind. Termine stehen noch nicht fest.


# alte Methoden neu vermittelt und angewandt

Eine fortlaufende Gruppe für Selbsterfahrung und Meditation. Wir treffen uns wöchentlich zum meditieren und um neue wie alte Techniken der Selbsterfahrung zu benutzen. Die Methoden sind zwar meist aus dem tibetischem Buddhismus entliehen aber viele haben schon langen ihren Weg in die moderne Psychotherapie gefunden. Dinge wie "innere Achtsamkeit" sind bestimmt dem einem oder der anderen bekannt doch dass ist nur die Haupttechnik der ältesten Schule. Durch meine lange Erfahrung mit dieser und späteren Techniken kann ich hoffentlich Neulingen und auch alten Hasen einen nützlichen Rahmen geben.

# Buddhismus - praktische Selbsterfahrung in alter Verpackung

Mit dieser Gruppe möchte ich bewusst mit einigen alten traditionellen Sichtweisen brechen und das zeitlose Potential alter Techniken neu vermitteln. Die innere Achtsamkeit ist nur eine Methode von vielen die völlig losgelöst von ihren alten Wurzeln in den Sutra-Belehrungen des Buddha heute selbst in sehr konservativen Kreisen der Psychotherapie Anwendung findet. Eine nicht so bekannte Methode ist die der Bewusstwerdung der Bewusstheit oder besser bekannt unter der englischen Bezeichnung "Awareness of Awareness". Hier wird eine Erfahrungsebene angesprochen die zu den fortgeschrittenen Methoden des tibetischem Buddhismus gehört, aber auch in anderen Traditionen zu finden ist.

# Diese Gruppe lebt von der Begegnung

Wir werden aber neben der stillen meditativen Selbsterfahrung auch viel in die Begegnung, den Austausch gehen. Bewegung, Ausdruck, Tanz und Stimme sprechen direkt tiefe Ebenen in uns an.

# Für wen ist diese Gruppe nun ...

In den vielen Jahren in denen ich mich mit Selbsterfahrung und Psychotherapie beschäftige, habe ich ständig Menschen getroffen die über diesen Weg zu "echten alten" Traditionen gefunden haben. Noch länger beobachte ich aber Menschen die aus der klassischen Meditation kommen und feststellen wie befreiend es sein kann den klassischen Rahmen zu verlassen und die gelernten Methoden einmal ohne den oft rigiden Rahmen einer Tradition anzuwenden.

Um es noch einmal deutlich zu sagen eine Selbsterfahrungsgruppe ersetzt nicht die bedingungslose Hingabe zu einem Meister. Aber die meisten Techniken die die Buddhas gelehrt haben benötigen diese Anbindung nicht und können losgelöst von der Tradition angewendet werden. Jeder noch so geheime klassische Methode ist heute im original im Internet zu finden. Es wird Zeit einen selbstbewussten und selbstverantwortlichen Umgang damit zu kultivieren.

Andererseits entstehen durch die klassische Meditationstechniken bei vielen Praktizierenden der Wunsch die gemachten neuen Eindrücke, Erfahrungen oder auftauchende Blockierungen unabhängig vom Rahmen einer Meditation zu integrieren. Hier sind die modernen Methoden der westlichen Psychologie nachweislich effektiv. Eine gute Balance aus alt und neu könnte also hilfreich sein.

# Termine

noch bin ich in der Planung und warte auf eine Lockerung der Kontaktbeschränkungen.
