---
title: 'Corona Hinweise'
date: 2021-11-28
description: ''
summary: '
Jeder trägt eine Mitverantwortung in einer Gemeinschaft, daher ist das Einhalten der grundlegenden
Empfehlungen und Verordnungen mir persönlich ein wichtiges Anliegen. Lesen Sie bitte den ganzen Artikel'
# menu: main
weight: 4
image: ''
featured: true
draft: true
publishdate: 2021-11-27
expirydate:
intro_image:
intro_image_absolute: true
intro_image_hide_on_mobile: true
---

# Corona Hinweise

{{< img-r s="./coronavirus.png" w="90px" >}}

## 2G-Regelung

Zum Schutze aller und zur sicheren Aufrechterhaltung meiner Dienstleistung ist die 2G-Regelung die Beste. Wenn
es sehr wichtige medizinische Gründe gibt, wegen derer Sie nicht geimpft sein können, teilen Sie mir diese bitte mit.
Ich versuche, Sie
dann ggf. am Ende des Tages mit Maske und zusätzlicher Lüftung zu empfangen. Wenn Sie Krankheitssymptome haben, bitte ich
Sie den Termin abzusagen, gerne auf per Textnachricht. Ich empfehle die Verwendung der "Corona-Warn App", da diese die
Kontaktverfolgung erleichtert.



## Einlass

Bitte seien Sie pünktlich zum vereinbarten Termin vor Ort, da ich Sie dann erst reinlassen kann,
 ein Wartebereich steht aktuell nicht zur Verfügung. Beim Betreten des Hauses
  und des Praxisraumes ist eine medizinische Maske zu tragen.

## Während des Gesprächs

Während des Gesprächs halten wir Abstand und der Raum ist vorher und währenddessen gelüftet, also ist eine Maske nicht
zwingend erforderlich.

## Mein Status

Ich selbst bin geimpft und auch genesen, daher ist es offenbar leider möglich, dass ich
Überträger sein könnte ohne Krankheitssymptome zu haben.
 Ich mache selbst regelmäßige Schnelltests, welche leider keine 100%-ige Garantie geben.
