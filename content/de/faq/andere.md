---
title: 'Sozialpädagogen u.a.'
discription:
date: 2020-05-01
menu:
summary: '
Bleiben noch die zu nennen, die ohne einen offiziellen Rahmen eine gute Ausbildung in einer anerkannten oder erfolgreichen
 Therapiemethode erlernt haben könnten. Das können Sozialpädagogen, Erzieher, Kindergärtner, Pfarrer,
  Krankenschwestern usw. sein. Diese Gruppen können auch meist nicht mit Kassen abrechnen.'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured:
weight: 7
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# Sozialpädagogen, Erzieher und Kindergärtner

Bleiben noch die zu nennen, die ohne einen offiziellen Rahmen eine gute Ausbildung in einer anerkannten oder erfolgreichen
 Therapiemethode erlernt haben könnten. Das können Sozialpädagogen, Erzieher, Kindergärtner, Pfarrer,
  Krankenschwestern usw. sein. Diese Gruppen können auch meist nicht mit Kassen abrechnen.
