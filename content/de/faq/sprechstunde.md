---
title: 'Sprechstunde "Seelische Gesundheit"'
discription:
date: 2021-11-11
menu:
summary: 'Belastet Sie die Arbeitslosigkeit durch finanzielle Sorgen und
Zukunftsängste oder hatten Sie im letzten Job Stress und fühlen sich noch belastet?
In Kooperation mit der Arbeitsargentur ist eine erste Hilfe möglich.
'
categories: [one]
tags: [two,three,four]
image: images/undraw/undraw_abstract_x68e.svg
draft: false
featured: true
weight: 5
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

## für Kund*Innen des Jobcenters Lübeck und Ost-Holstein

Haben Sie etwas auf der Seele? Eine private Krise? Mobbing erlebt?
Fühlen Sie sich in einer Sackgasse oder gefangen im Gedankenkarussell?
Suchen Sie ein offenes Ohr und ein vertrauliches Gespräch?
Dann Fragen Sie Ihren Vermittler nach einem Beratungsgutschein.

Vereinbaren Sie kostenfrei und diskret einen Termin in der Sprechstunde für
seelische Gesundheit und geben hierbei bitte an, dass Sie Kundin/Kunde des
Jobcenters sind, damit Sie das Gesprächs- und Beratungsangebot kostenfrei nutzen können.

Erfahren sie mehr unter [hier](https://sprechstunde-seelische-gesundheit.de/)
