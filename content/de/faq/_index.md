---
title: 'Fakten'
discription:
date: 2020-05-01
menu: main
summary:
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 1
weight_front:
weight_print:
intro_image: images/undraw/undraw_personal_text_vkd8.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# kurze Antworten auf oft gestellte Fragen

Wenn Sie nur wenige Vorstellungen darüber haben, welche Therapieformen es
gibt und welche zu Ihnen und Ihren Änderungswünschen passen könnte, möchte
ich Ihnen hier einen groben Überblick geben. Auch über Berufsbilder bzw.
Bezeichnungen gibt es einiges zu wissen. Wie Sie die Kosten ggf. erstattet
bekommen können, finden Sie demnächst auch hier.
