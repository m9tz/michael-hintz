---
title: 'Mental+'
discription: 'Integrationscoaching Mental Plus für Menschen mit psychosozialen Problemstellungen'
date: 2024-08-08
menu:
summary: 'Lebenskrisen, psychosoziale Schwierigkeiten und psychische Erkran­kungen: Du quälst dich schon eine ganze Weile damit rum, diesen Ballast abzuschütteln. Inzwischen fragst du dich, wie du jemals wieder einer geregelten Arbeit nachgehen sollst. Keine Angst, mit dieser Frage stehst du nicht allein da. Es geht vielen ähnlich. Und es kann viele Gründe dafür geben. Häufig fehlen nur eine helfende Hand und ein offenes Ohr.
'
categories: [one]
tags: [two,three,four]
image: images/undraw/undraw_abstract_x68e.svg
draft: false
featured: true
weight: 5
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

## für Kund*Innen des Jobcenters Lübeck und Ost-Holstein

Wie man lernt, mit seinen Eigenheiten zurechtzukommen.

Lebenskrisen, psychosoziale Schwierigkeiten und psychische Erkran­kungen: Du quälst dich schon eine ganze Weile damit rum, diesen Ballast abzuschütteln. Inzwischen fragst du dich, wie du jemals wieder einer geregelten Arbeit nachgehen sollst. Keine Angst, mit dieser Frage stehst du nicht allein da. Es geht vielen ähnlich. Und es kann viele Gründe dafür geben. Häufig fehlen nur eine helfende Hand und ein offenes Ohr.

Manchmal steht man sich selbst im Weg, ohne es zu merken. Wenn man dann Hilfe bekommen kann, dann sollte man sie in Anspruch nehmen.

Weißt du oder vermutest du, dass Folgendes oder Ähnliches auf dich zutreffen könnte?

• Plagen dich Ängste?
• Fühlst du dich manchmal depressiv?
• Lebst du in persönlich schwierigen Verhältnissen?
• Fühlst du dich psychisch instabil?
• Konsumierst du Suchtmittel? Ist dieser sogar gestiegen?
• Gibt es etwas, dass du alleine gerade nicht bewältigen kannst?
• Leidest du unter Essstörungen?

Damit musst du dich nicht abfinden. Wirklich nicht!
Lass uns einfach unter vier Augen darüber sprechen.
Persönlich vor Ort oder online, was dir lieber ist.

Mögliche Inhalte je nach Bedarf:
• Krisen überwinden
• Wieder auf eigenen Füßen stehen
• Begreifen der eigenen Situation
• Bedeutsamkeit für das eigene Leben
• Erfahren, wo man Hilfe bekommt
• Sagen, was man meint
• Verdeutlichen, was man braucht
• uvm.

Wichtig dabei ist die Entwicklung bzw. Förderung und Konsolidierung von psychosozialen Schlüsselkompetenzen für den beruflichen Alltag. Dies ermöglicht es dir, sich auf die wachsenden Anforderungen z. B. im Bereich der Selbstorganisation, Problemlösung und Stressbewältigung in der Arbeitswelt vorzubereiten.

Bei der Umsetzung finden deine individuellen körperlichen und mentalen Fähigkeiten Berücksichtigung.

Der regelmäßige Kontakt zu einer festen Bezugsperson unterstützt dich, deine sozialen Kompetenzen wie Kommunikationsfähigkeit, Konfliktfähigkeit, Teamfähigkeit, Kontaktfähigkeit, Anpassungsfähigkeit, Umstellungsvermögen usw. zu überprüfen, vorhandene Ressourcen und den eigenen Förderbedarf zu erkennen und Motivation für den individuellen Förderplan zu entwickeln. Damit wird eine grundlegende Voraussetzung für eine erfolgreiche soziale und berufliche (Re-)Integration geschaffen.

Weiterhin erfolgt eine erste Überprüfung, ob der Verdacht einer behandlungsbedürftigen Erkrankung vorliegt. Du wirst in diesem Fall an Beratungs-, und Behandlungsmöglichkeiten herangeführt, die Compliance für eine umfängliche Abklärung wird gefördert und ggf. erfolgt eine Begleitung zu Ärzten und/ oder Psychotherapeuten oder anderen Behandlungsmöglichkeiten.

Erfahren sie mehr im KursNet der [Arbeitsargentur](https://web.arbeitsagentur.de/coachingundaktivierung/suche/aktivierungsangebot/167433222?mz=SA%2001&uk=Bundesweit&pg=0&ssw=mental%20plus)

oder direkt bei dem [Massnahmenträger](https://mental-plus.de/integrationscoaching)

