---
title: 'Heilpraktiker:innen'
discription:
date: 2020-02-11
menu:
summary: '
Im Bereich der Psychotherapie bin ich zwar vorwiegend tätig, aber einen Blick auf all die anderen
Felder eines gesunden und ausgewogenen Lebens zu haben, unterscheidet den Voll-Heilpraktiker von anderen Heilberufen.'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 1
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---



# Heilpraktiker:innen

In Deutschland gibt es schon sehr lange den Beruf des Heilpraktikers. Dieser kann eine Therapiemethode erlernt haben, wie z.B. eine psychotherapeutische.
 Seit vielen Jahren gibt es den Heilpraktiker speziell für Psychotherapie, dieser darf ausschließlich
  psychotherapeutisch tätig sein, also nicht auch zusätzlich Heilpflanzen oder Massagen anbieten.
   Bei allen Heilpraktiker:innen ist es wichtig, zu wissen, dass die Berufsbezeichnung per se
    nichts über die Qualität dieser Person als Anwender:in der Methode "Psychotherapie" aussagt.
     Die Zulassung als Heilpraktiker:in bestätigt zunächst nur, dass diese:r die gesetzlichen Grenzen kennt
      und für Handlungen haftet, die medizinisch nicht in sein Aufgabenfeld gehören.
      Wenn er oder sie jedoch nachweisen kann, dass eine bestimmte Methode beherrscht wird, darf diese auch verwendet werden.
       Heilpraktiker:innen können gewisse Leistungen mit einigen Kassen abrechnen,
         in der Regel jedoch keine psychotherapeutischen.

Der Beruf des Heilpraktikers ist sehr beliebt, weil er, anders als der des Arztes,
 nicht an derartig enge Vorstellungen von Wissenschaftlichkeit gebunden ist.
  Das betrachte ich persönlich als Vorteil. Nicht nur in der Psychotherapie ist unsere aktuelle, westliche Vorstellung von der Welt, der Biologie,
    des Lebens und der Psyche nicht immer ausreichend um zu heilen. Damit möchte ich in keiner Weise
     die riesigen Errungenschaften klein reden, die seit des Zeitalters der Aufklärung Einzug gehalten haben.
      Gerade die Psyche ist es aber, die besonders in den alten Hochkulturen Asiens schon seit Jahrtausenden erforscht wird.
   Viele dieser alten Techniken und Ansichten bilden heute die Grundlage einiger "moderner" Therapiemethoden.
    Bei der Integration dieser am Anfang oft als unwissenschaftlich betrachteten Verfahren leisten die Heilpraktiker:innen
     einen wichtigen Beitrag.
