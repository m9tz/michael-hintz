---
title: 'Psycholog:innen'
discription:
date: 2020-01-20
menu:
summary: 'Ein:e Psycholog:in ist jemand, der Psychologie studiert hat,
 das ist zunächst eine recht wissenschaftliche Ausbildung.'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: false
weight: 15
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Psycholog:innen

Ein:e Psycholog:in ist jemand, der Psychologie studiert hat. Das ist zunächst eine recht wissenschaftliche Ausbildung.
 Dieser Mensch kann dann noch eine mehrjährige Ausbildung in einer oder mehreren Psychotherapiemethoden absolviert haben.
  Er oder sie kann sich dann psychologische:r Psychotherapeut:in nennen.
   Dieser Titel ist geschützt und wird meist freiberuflich ausgeübt.
    Wenn dieser Mensch zudem Methoden erlernt hat,
     die deutsche Krankenversicherungen für hilfreich halten,
      dann könnte Ihnen eine mit der Krankenkasse abrechenbare Psychotherapie angeboten werden.
       Voraussetzung hierfür ist eine Kassenzulassung und freie Kapazitäten in der Praxis.
