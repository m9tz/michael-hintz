---
title: 'Erstgespräch'
discription:
date: 2020-04-01
menu:
summary: '
In einem ersten Gespräch geht es darum, sich kennen zu lernen. Danach können weiter Probesitzungen oder eine Bedenkzeit
 vereinbart werden. Die Beziehung kann natürlich weiter entwickelt werden, aber ein erster persönlicher Eindruck ist
  eine echte Entscheidungsgrundlage für das weitere Vorgehen.'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 3
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Im ersten Gespräch


In einem ersten Gespräch geht es darum, sich kennen zu lernen. Danach können weiter Probesitzungen oder eine Bedenkzeit
 vereinbart werden. Die Beziehung kann natürlich weiter entwickelt werden, aber ein erster persönlicher Eindruck
  ist eine echte Entscheidungsgrundlage für das weitere Vorgehen.
