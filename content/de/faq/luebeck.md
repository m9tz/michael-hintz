---
title: 'Hilfsangebote in Lübeck'
discription:
date: 2021-11-26
menu:
summary: '
Einige Hilfsangebote in Lübeck und Umgebung von Notfallnummern bis Einrichtungen für stationäre Hilfe'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 2
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# Angebote in Lübeck

Bei akuten Notfällen und auch als weiterführende Hilfestellungen, möchte ich hier einige Internetseiten
aufliste, die hilfreich sein könnten.

In jedem Fall ist die wichtigste Notrufnummer die 112.

Sollte diese Nummer nicht funktionieren oder ein Krankentransport benötigt
werden, ist die Nummer 0451-19222 die richtige. Bei Straftaten ist die 110 zuständig, die in Lübeck
auch über 0451-1310 erreichbar ist.
Ein Hilfetelefon für Gewalt gegen Frauen und Kindern steht unter 08000-116016 zur Verfügung.


* [Notdienste](https://www.luebeck.de/de/notdienste/index.html)

* [Psychosozialer Wegweiser Lübeck](https://www.psychosozialer-wegweiser-luebeck.de)

* [Telefonseelsorge](https://www.telefonseelsorge-luebeck.de/) Tel: 0800 111 0 111
