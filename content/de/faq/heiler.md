﻿---
title: 'Heiler:innen'
discription:
date: 2020-05-15
menu:
summary:
categories: [one]
tags: [two,three,four]
image:
draft: true
featured:
weight: 7
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# Heiler:innen

Eine neue Berufsbezeichnung ist der Heiler oder Heilerin diese/ dieser macht z.B. Fernheilungen, Hand
 auflegen oder Gebetsheilung. Das hört sich zunächst sehr esoterisch an, ist es aber auch. Wer hat nicht schon
  von einer erfolgreichen Behandlung von Warzen durch Besprechen gehört? Das Feld ist vielfältig
   und greift auch in die Psyche ein. Hier ist zu beachten, dass Heilende
    nicht wie Heilpraktiker:innen eine gute Grundkenntnis der medizinisch machbaren Methoden und mitunter meldepflichtigen
     Krankheiten haben. Eine nicht erkannte ansteckende Krankheit kann weitreichende Folgen haben.
      Die eigene Verantwortung sollte niemals an der Garderobe abgegeben werden, weder beim Arzt noch beim Heiler.
