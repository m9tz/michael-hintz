---
title: 'Web Meeting'
date: 2020-05-16
description: ''
summary: '
Beratung und Coaching sind nach Absprache auch telefonisch oder per Videokonferenz möglich.'
# menu: main
weight: 4
image: ''
featured: true
draft: true
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

Beratung und Coaching sind nach Absprache auch telefonisch oder per Videokonferenz möglich.
Eine eigene Serverlösung ist in Vorbereitung, bis dahin benutze ich die "opensource" Plattform
[Jitsi](https://jitsi.org/) als sicheren Kommunikationsweg.

# Erstgespräch

Ein erster physischer Kontakt ist zwingend erforderlich, bevor eine Fortführung der
Behandlung durch andere Medien stattfinden darf.


# Online Psychotherapie

Die direkte körperliche Interaktion und die feinsten Eindrücke sind online noch nicht möglich.
Was für Sie zielführend und hilfreich ist, müssen Sie selber feststellen. Eine gute Ausgewogenheit
zwischen physischen und virtuellen Sitzungen muss gemeinsam abgestimmt werden.

# Technische Umsetzung

Sie benötigen nur einen modernen Web-Browser mit Zugang zu Audio- und Video-Endgeräten.
Es ist auch möglich eine App für ein Tablet oder Handy zu installieren.
