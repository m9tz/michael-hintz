﻿---
title: 'Abrechnung mit Kassen'
discription:
date: 2020-05-14
menu:
summary: '
Der Heilpraktiker kann meist nicht direkt mit Krankenkassen abrechnen. Nur in besonderen Fällen kann ich
mit einigen privaten Voll- oder Zusatzversicherungen abrechnen.'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: true
weight: 3
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# Krankenkassen

Der Heilpraktiker kann nicht direkt mit Krankenkassen abrechnen. Nur in besonderen Fällen
kann mit einigen privaten Voll- oder Zusatzversicherungen abgerechnet werden. Daher ist in den überwiegenden
Fällen davon auszugehen, dass die Kosten selbst getragen werden müssen.



## Weitere Möglichkeiten

Die Abrechnung mit Kassen ist aber nicht die einzige Methode, eine notwendige Psychotherapie zu finanzieren.
Dazu entsteht demnächst eine weitere Seite, weil es an einem Überblick fehlt und es immer wichtiger wird,
die Finanzierung durch andere Träger zu realisieren.
