---
title: 'Psychiater:innen'
discription:
date: 2020-03-12
menu:
summary: 'Auch Psychiater:innen können mit der Kasse abrechnen. Diese sind Fachärzte und studierte Mediziner:innen,
 z.B. mit der
Fachrichtung "Psychotherapie".'
categories: [one]
tags: [two,three,four]
image:
draft: false
featured: false
weight: 15
weight_front:
weight_print:
intro_image:
intro_image_absolute: false
intro_image_hide_on_mobile: true
---


# Psychiater:innen

Auch Psychiater:innen können mit der Kasse abrechnen. Diese sind Fachärzte und studierte Mediziner:innen,
 z.B. mit der
Fachrichtung "Psychotherapie". Aber auch andere Fachärzte bieten Psychotherapie an, z.B. Fachärzte
für "Kinder- und Jugendpsychiatrie und Psychotherapie". Sie merken sicherlich bereits, dass dieses Feld etwas
unübersichtlich ist. Ob jemand Erfolge als Therapeut:in erzielt, hängt nur zum Teil von dessen Ausbildung
ab, vielmehr aber, so meine ich, von seiner Persönlichkeit und dem Kontakt, der zwischen Ihnen und dem Therapeuten entsteht.
